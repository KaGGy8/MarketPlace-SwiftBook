//
//  AppDelegate.swift
//  MarketPlace-SwiftBook
//
//  Created by Сергей Вихляев on 18.12.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  private let tokenCheckerManager: ITokenCheckerManagement

  override init() {
    self.tokenCheckerManager = TokenCheckerManager(
      keychainStorageService: KeychainStorageService.shared
    )
    super.init()
  }

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    tokenCheckerManager.checkTokenInLocalStorage(for: "") { result in
      switch result {
      case .success:
        routeToMainApp()
      case .failure(let error):
        print(error.localizedDescription)
        createAndShowStartVC()
      }
    }

    return true
  }

  /// Для смены рут экрана, входа и выхода из аккаунта
  /// - Parameters:
  ///   - vc: на какой экран делать переход
  ///   - animated: с анимацией переход или нет ( по умолчанию анимированный )
  func changeRootViewController(_ vc: UIViewController,
                                animated: Bool = true) {
    guard let window = window else { return }

    window.rootViewController = vc

    UIView.transition(with: window,
                      duration: 0.5,
                      options: [.showHideTransitionViews],
                      animations: nil,
                      completion: nil)
  }
}

private extension AppDelegate {

  func routeToMainApp() {
    let tabViewController = TabBarViewController()
    let mainNavigationController = UINavigationController(rootViewController: tabViewController)
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = mainNavigationController
    window?.makeKeyAndVisible()
  }

  func createAndShowStartVC() {
    let startVC = LoginScreenViewController(
      nibName: String(describing: LoginScreenViewController.self),
      bundle: nil
    )

    let navigationController = UINavigationController(rootViewController: startVC)
    let loginAssembly = LoginAssembly(navigaionController: navigationController)
    loginAssembly.configure(viewController: startVC)

    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = navigationController
    window?.makeKeyAndVisible()
  }
}
