//
//  MVXProtocols.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation
import UIKit

protocol BaseAssembly {
  func configure(viewController: UIViewController)
}

protocol BaseRouting {
  func routeTo(target: Any)
  init(navigationContoller: UINavigationController)
}
