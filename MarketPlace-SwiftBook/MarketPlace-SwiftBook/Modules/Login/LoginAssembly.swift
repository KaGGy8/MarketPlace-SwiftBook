//
//  LoginAssembly.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation
import UIKit

final class LoginAssembly {

  private let navigaionController: UINavigationController

  init(navigaionController: UINavigationController) {
    self.navigaionController = navigaionController
  }
}

extension LoginAssembly: BaseAssembly {
  func configure(viewController: UIViewController) {
    guard let loginScreenViewController = viewController as? LoginScreenViewController else { return }

    let presenter = LoginPresenter()
    let router = LoginRouter(navigationContoller: navigaionController)

    loginScreenViewController.presenter = presenter
    presenter.router = router
  }
}
