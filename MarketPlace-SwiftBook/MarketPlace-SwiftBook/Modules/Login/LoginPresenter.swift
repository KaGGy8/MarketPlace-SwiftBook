//
//  LoginPresenter.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation

protocol LoginPresentation {
  func runRegistratonFlow()
}

final class LoginPresenter {
  var router: LoginRouting?
}

// MARK: - LoginPresentation
extension LoginPresenter: LoginPresentation {
  func runRegistratonFlow() {
    router?.routeTo(target: LoginRouter.Targets.registration)
  }
}
