//
//  BasketAssembly.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 02.04.2023.
//

import Foundation
import UIKit

final class BasketAssembly {
  private let navigaionController: UINavigationController

  init(navigaionController: UINavigationController) {
    self.navigaionController = navigaionController
  }
}

extension BasketAssembly: BaseAssembly {
  func configure(viewController: UIViewController) {
    guard let basketViewController = viewController as? BasketViewController else { return }
    let presenter = BasketPresenter()
    let router = BasketRouter(navigationContoller: navigaionController)
    basketViewController.presenter = presenter
    presenter.router = router
  }
}
