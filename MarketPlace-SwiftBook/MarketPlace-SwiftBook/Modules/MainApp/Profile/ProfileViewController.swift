//
//  ProfileViewController.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 26.02.2023.
//

import UIKit

// Экран профиля
final class ProfileViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!



  override func viewDidLoad() {
    super.viewDidLoad()
    registerXibs()
    tableView.delegate = self
    tableView.dataSource = self
  }
}

private extension ProfileViewController {
  func registerXibs() {
    let xibNames = [
      String(describing: AccountTableViewCell.self),
      String(describing: LikesTableViewCell.self),
      String(describing: MyOrdersTableViewCell.self),
      String(describing: AppSettingsTableViewCell.self),
      String(describing: LogoutTableViewCell.self)
    ]

    xibNames.forEach { (name) in
      tableView.register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: name)
    }
  }
}

extension ProfileViewController: UITableViewDelegate {
  func tableView(
    _ tableView: UITableView,
    didSelectRowAt indexPath: IndexPath
  ) {
  }
}

extension ProfileViewController: UITableViewDataSource {
  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    6
  }

  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    switch indexPath.row {
    case 0:
      let cell = tableView
        .dequeueReusableCell(
          withIdentifier: String(describing: AccountTableViewCell.self),
          for: indexPath
        ) as! AccountTableViewCell
      return cell
    case 1:
      let cell = tableView
        .dequeueReusableCell(
          withIdentifier: String(describing: LikesTableViewCell.self),
          for: indexPath
        ) as! LikesTableViewCell
      return cell
    case 2:
      let cell = tableView
        .dequeueReusableCell(
          withIdentifier: String(describing: MyOrdersTableViewCell.self),
          for: indexPath
        ) as! MyOrdersTableViewCell
      return cell
    case 3:
      let cell = UITableViewCell()
      cell.backgroundColor = .gray
      return cell
    case 4:
      let cell = tableView
        .dequeueReusableCell(
          withIdentifier: String(describing: AppSettingsTableViewCell.self),
          for: indexPath
        ) as! AppSettingsTableViewCell
      return cell
    case 5:
      let cell = tableView
        .dequeueReusableCell(
          withIdentifier: String(describing: LogoutTableViewCell.self),
          for: indexPath
        ) as! LogoutTableViewCell
      return cell
    default:
      return UITableViewCell()
    }
  }

  func tableView(
    _ tableView: UITableView,
    heightForRowAt indexPath: IndexPath
  ) -> CGFloat {
    switch indexPath.row {
    case 0, 5:
      return 100
    default:
      return 60
    }
  }
}
