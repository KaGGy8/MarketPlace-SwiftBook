//
//  RegistrationAssembly.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 12.02.2023.
//

import Foundation
import UIKit

final class RegistrationAssembly {

  private let navigaionController: UINavigationController

  init(navigaionController: UINavigationController) {
    self.navigaionController = navigaionController
  }
}

extension RegistrationAssembly: BaseAssembly {
  func configure(viewController: UIViewController) {
    guard let registrationScreenViewController = viewController as? RegistrationScreenViewController else { return }

    let presenter = RegistrationPresenter()
    let router = RegistrationRouter(navigationContoller: navigaionController)

    registrationScreenViewController.presenter = presenter
    presenter.router = router
  }
}
