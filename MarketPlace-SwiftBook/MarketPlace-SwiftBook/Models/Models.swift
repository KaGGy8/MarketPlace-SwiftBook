//
//  AccountModel.swift
//  MarketPlace-SwiftBook
//
//  Created by Sergei Vikhliaev on 22.01.2023.
//

import Foundation

/// Модель заказа
struct OrderModel: Equatable {
  var productsAndCount: [Product: Int]
}

/// Модель Продукта
struct Product: Hashable {

}

/// Модель пользовательских настроек
struct AccountSettings: Equatable {
  var isDarkMode: Bool

  init() {
    self.isDarkMode = false // системный
  }
}

struct PersonalInfo: Equatable {
  var nickName: String
  var name: String
  var email: String
}

/// Модель аккаунта пользователя
struct AccountModel: Equatable {
  var id: Int?
  var personalInfo: PersonalInfo?
  var orders: [OrderModel] = []
  var favorites: [Product] = []
  var settings: AccountSettings = AccountSettings()

  static func emptyModel() -> AccountModel {
    return AccountModel(id: nil,
                        personalInfo: nil,
                        orders: [],
                        favorites: [],
                        settings: AccountSettings())
  }

  static func == (lhs: AccountModel, rhs: AccountModel) -> Bool {
    return lhs.id == rhs.id &&
    lhs.personalInfo == rhs.personalInfo &&
    lhs.orders == rhs.orders &&
    lhs.favorites == rhs.favorites &&
    lhs.settings == rhs.settings
  }
}
