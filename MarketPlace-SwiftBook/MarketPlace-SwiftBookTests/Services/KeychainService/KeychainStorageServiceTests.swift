//
//  KeychainStorageServiceTests.swift
//  MarketPlace-SwiftBookUITests
//
//  Created by Дмитрий Данилин on 18.01.2023.
//

import XCTest
@testable import MarketPlace_SwiftBook

final class KeychainStorageServiceTests: XCTestCase {
    var keychainStorageService: IKeychainStorageService?
    
    let accessToken = Account(accessToken: "dummy-access-token")
    let service = "foo"
    let account = "bar"
    
    override func setUp() {
        keychainStorageService = ServiceAssembly().keychainStorageService
    }
    
    override func tearDown() {
        clearToken(service, account)
        keychainStorageService = nil
    }
    
    // MARK: - Try save token
    func test_save_shouldStatusOnSuccess() {
        var success: Bool = false
        
        keychainStorageService?.save(
            accessToken,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success():
                    success = true
                case .failure(let error):
                    XCTFail(error.localizedDescription)
                }
            }
        )
        
        XCTAssertTrue(success)
        
    }
    
    func test_save_doubleTokenSave_shouldStatusOnErrSecDuplicateItem() {
        var success: Bool = false
        var statusError: Error?
        
        keychainStorageService?.save(
            accessToken,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success():
                    success = true
                case .failure(let error):
                    switch error {
                    case .unknownError(let error):
                        XCTFail(error.localizedDescription)
                    default:
                        break
                    }
                }
            }
        )
        
        keychainStorageService?.save(
            accessToken,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success():
                    success = false
                case .failure(let error):
                    switch error {
                    case .errSecDuplicateItem:
                        statusError = error
                    case .unknownError(let error):
                        XCTFail(error.localizedDescription)
                    default:
                        XCTFail()
                    }
                }
            }
        )
        
        XCTAssertTrue(success, "Повторного получения данных быть не должно")
        XCTAssertNotNil(statusError)
    }
    
    // MARK: - Try fetch token
    func test_fetch_shouldStatusOnSuccess() {
        var token: Account?
        
        createdToken(accessToken, service, account)
        keychainStorageService?.fetch(
            typeData: Account.self,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success(let data):
                    token = data
                case .failure(let error):
                    XCTFail(String(describing: error))
                }
            }
        )
        
        XCTAssertNotNil(token)
        XCTAssertEqual(accessToken, token)
    }
    
    func test_fetch_nonExistentData_shouldStatusOnErrSecItemNotFound() {
        var statusError: Error?
        
        keychainStorageService?.fetch(
            typeData: Account.self,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success(_):
                    XCTFail("Данных быть не должно")
                case .failure(let error):
                    switch error {
                    case .errSecItemNotFound:
                        statusError = error
                    case .unknownError(let error):
                        XCTFail(error.localizedDescription)
                    default:
                        XCTFail()
                    }
                }
            }
        )
        
        XCTAssertNotNil(statusError)
    }
    
    // MARK: - Try update token
    func test_update_shouldStatusOnSuccess() {
        var success: Bool = false
        
        createdToken(accessToken, service, account)
        keychainStorageService?.update(
            accessToken,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success():
                    success = true
                case .failure(let error):
                    XCTFail(String(describing: error))
                }
            }
        )
        
        XCTAssertTrue(success)
        
    }
    
    func test_update_nonExistentData_shouldStatusOnErrSecItemNotFound() {
        var statusError: Error?
        
        keychainStorageService?.update(
            accessToken,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success(_):
                    XCTFail("Обновления токена не должно срабатывать")
                case .failure(let error):
                    switch error {
                    case .errSecItemNotFound:
                        statusError = error
                    case .unknownError(let error):
                        XCTFail(error.localizedDescription)
                    default:
                        XCTFail()
                    }
                }
            }
        )
        
        XCTAssertNotNil(statusError)
    }
    
    // MARK: - Try delete token
    func test_delete_shouldStatusOnSuccess() {
        var operationStatus: Bool = false
        
        createdToken(accessToken, service, account)
        keychainStorageService?.delete(
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success():
                    operationStatus = true
                case .failure(let error):
                    XCTFail(String(describing: error))
                }
            }
        )
        
        XCTAssertTrue(operationStatus)
    }
    
    func test_delete_nonExistentData_shouldStatusOnErrSecItemNotFound() {
        var statusError: Error?
        
        keychainStorageService?.delete(
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success():
                    XCTFail("Удаление токена не должно срабатывать")
                case .failure(let error):
                    switch error {
                    case .errSecItemNotFound:
                        statusError = error
                    case .unknownError(let error):
                        XCTFail(error.localizedDescription)
                    default:
                        XCTFail()
                    }
                }
            }
        )
        
        XCTAssertNotNil(statusError)
    }
    
    // MARK: - Private methods
    private func createdToken(
        _ accessToken: Account,
        _ service: String,
        _ account: String
    ) {
        keychainStorageService?.save(
            accessToken,
            service: service,
            account: account,
            completion: { result in
                switch result {
                case .success():
                    break
                case .failure(let error):
                    XCTFail(String(describing: error))
                }
            }
        )
    }
    
    private func clearToken(
        _ service: String,
        _ account: String
    ) {
        keychainStorageService?.delete(
            service: service,
            account: account,
            completion: { _ in }
        )
    }
}
